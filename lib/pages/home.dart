import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
class Home extends StatefulWidget {
  // final Socket channel;

  // const Home({Key key, this.channel}) : super(key: key);
  
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int _counter=0;

  void _incrementar(){
    setState(() {
      _counter++;
    });
  }

  // void _chamarcanal(){
  //   widget.channel.write("POWWWWWWW\n");
  // }

  // @override
  // void fecharcanal(){
  //   widget.channel.close();
  //   super.dispose();
  // }
MaterialColor _corbotao1=Colors.red;
MaterialColor _corbotao2=Colors.red;
Timer _timer;
int _start = 5;

void startTimer(usuario){
  const oneSec = const Duration(seconds: 1);
  _timer = new Timer.periodic(
    oneSec,
    (Timer timer) => setState(
      () {
        print(usuario);
        if(usuario==3){
          timer.cancel();
          _start=0;
        }else{
          if (_start < 1) {
            if(usuario==1){
              timer.cancel();
              _start=5;
              startTimer(2);
            }else{
              timer.cancel();
              _start=5;
              startTimer(1);
            }
          } else {
            _start = _start - 1;
          }
        }
      },
    ),
  );
  if(usuario==1){
    _corbotao1=Colors.blue;
    _corbotao2=Colors.red;
  }else{
    _corbotao1=Colors.red;
    _corbotao2=Colors.blue;
  }
}

@override
void dispose() {
  _timer.cancel();
  super.dispose();
}
resetartimer(){
  _timer.cancel();
  _start=5;
}
Widget build(BuildContext context) {
  _portraitModeOnly();
  return new Scaffold(
    appBar: AppBar(title: Text("Contador Xadrez")),
    body:
    Column(children: <Widget>[
 Container( 
          width: 150,
          height:50,
          margin: EdgeInsets.all(40),
          // padding: EdgeInsets.only(right: 40),
          child: 
            RaisedButton(
              color:Colors.green,
              onPressed: () {
                resetartimer();
              },
              child: Text("Reset"),
            ),
        ),
    Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(horizontal:100),
          child:Center(
            child:RaisedButton(
              color:_corbotao1,
              onPressed: () {
                _corbotao1=Colors.blue;
                startTimer(1);
              },
              child: Text("Usuario 1"),
            ),
          )
        ),
        Text("$_start"),
        Padding(
          padding: EdgeInsets.symmetric(horizontal:100),
          child:Center(
            child:
            RaisedButton(
              color:_corbotao2,
              onPressed: () {
                _corbotao2=Colors.blue;
                startTimer(2);
              },
              child: Text("Usuario 2"),
            ),
          )
        ),
        // Padding(
        //   padding: EdgeInsets.symmetric(vertical:100),
        //   child:Center(
        //     child:
        //     RaisedButton(
        //       color:Colors.green,
        //       onPressed: () {
        //         resetartimer();
        //       },
        //       child: Text("Reset"),
        //     ),
        //   )
        // ),
        
      ],
    ),
    ],) 
   
  );
}
  void _portraitModeOnly() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
  ]);
  }
}
