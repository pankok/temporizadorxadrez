import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:io';
import 'package:temporizador_xadrez/pages/home.dart';

void main() async{
  // Socket sock;
  // sock = await Socket.connect('192.168.1.104', 80);
  runApp(MaterialApp(
      darkTheme: ThemeData(
        primaryColor: Color.fromRGBO(109, 23, 255, 1),
        accentColor: Color.fromRGBO(72, 74, 126, 1),
        brightness: Brightness.dark,
      ),
      theme: ThemeData(
        primaryColor: Color.fromRGBO(109, 23, 255, 1),
        accentColor: Color.fromRGBO(22, 43, 229, 1),
        brightness: Brightness.light,
      ),
      home:Home()
    ));
} 

// class MyApp extends StatelessWidget {
//   // Socket socket;

//   // MyApp(Socket sock) {
//   //   this.socket = sock;
//   // }
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
      
//       darkTheme: ThemeData(
//         primaryColor: Color.fromRGBO(109, 23, 255, 1),
//         accentColor: Color.fromRGBO(72, 74, 126, 1),
//         brightness: Brightness.dark,
//       ),
//       theme: ThemeData(
//         primaryColor: Color.fromRGBO(109, 23, 255, 1),
//         accentColor: Color.fromRGBO(22, 43, 229, 1),
//         brightness: Brightness.light,
//       ),
//       home:Home()
//     );
//   }
// }
